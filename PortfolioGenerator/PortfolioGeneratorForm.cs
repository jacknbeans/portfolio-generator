﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PortfolioGenerator
{
    public struct Document
    {
        public string Path;
        public string HtmlPath;
        public FileStream HtmlFile;
    }

    public partial class PortfolioGeneratorForm : Form
    {
        private const string FileType = ".jsf";
        private const string HtmlType = ".html";

        private readonly Color _statusErrorColor = Color.DarkRed;
        private readonly Color _statusSuccessColor = Color.DarkGreen;
        private readonly Color _statusWarningColor = Color.DarkGoldenrod;

        private readonly string _documentTemplate = "<!DOCTYPE html>" + Environment.NewLine +
                                                    "<html>" + Environment.NewLine;

        private const string DocumentTemplateEnding = "</html>";

        private const string DataPath = "\\data\\";
        private const string HeadFile = "head.html";
        private const string MenuLargeFile = "menu-large.html";
        private const string MenuMediumSmallFile = "menu-medium-small.html";
        private const string TitleTagFile = "title-tag.html";
        private const string FooterFile = "footer.html";

        public PortfolioGeneratorForm()
        {
            InitializeComponent();
        }

        private void PortfolioGeneratorForm_Load(object sender, EventArgs e)
        {
        }

        private void ButtonRootFolderBrowse_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = BrowseRootFolder.ShowDialog();
            if (dialogResult.Equals(DialogResult.OK))
            {
                TextBoxRootFolder.Text = BrowseRootFolder.SelectedPath;
            }
        }

        private void ButtonGenerate_Click(object sender, EventArgs e)
        {
            ProgressGeneration.Value = 0;
            TextBoxStatus.Clear();
            List<Document> documents = GetDocuments();

            ProgressGeneration.Step = ProgressGeneration.Maximum / documents.Count;
            foreach (Document document in documents)
            {
                try
                {
                    SetupDocument(document);
                    AddStatusTextSuccess($"Finished setting up file {document.HtmlPath}!");
                }
                catch (Exception exception)
                {
                    AddStatusTextError(exception.Message);
                }

                ProgressGeneration.PerformStep();
            }

            AddStatusTextSuccess("Finished!");
            ProgressGeneration.PerformStep();
        }

        private List<Document> GetDocuments()
        {
            List<Document> results = new List<Document>();

            Document document = new Document();
            foreach (string entry in Directory.EnumerateFileSystemEntries(TextBoxRootFolder.Text))
            {
                FileAttributes attributes = File.GetAttributes(entry);
                if (attributes.HasFlag(FileAttributes.Directory))
                {
                    string[] subFiles = Directory.GetFiles(entry);
                    foreach (string file in subFiles)
                    {
                        CheckAndAddFileSystemEntryToDocuments(file, results, document);
                    }
                }
                else
                {
                    CheckAndAddFileSystemEntryToDocuments(entry, results, document);
                }
            }

            return results;
        }

        private void CheckAndAddFileSystemEntryToDocuments(string entry, List<Document> documents, Document document)
        {
            if (entry.Contains(FileType))
            {
                AddStatusText($"Found file: {GetFileName(entry)}");
                string newFilePath = entry.Replace(FileType, HtmlType);

                if (File.Exists(newFilePath))
                {
                    FileAttributes attributes = File.GetAttributes(newFilePath);
                    if (attributes.HasFlag(FileAttributes.ReadOnly))
                    {
                        AddStatusTextWarning(
                            $"Could not create file {GetFileName(newFilePath)}, already exists and is read-only!");
                        return;
                    }
                }

                document.Path = entry;
                document.HtmlPath = newFilePath;

                bool success = true;
                try
                {
                    document.HtmlFile = new FileStream(newFilePath, FileMode.Create, FileAccess.Write);
                }
                catch (Exception e)
                {
                    AddStatusTextError($"Exception creating file {GetFileName(newFilePath)}: {e.ToString()}");
                    AddStatusTextError($"Details: {e.Message}");
                    success = false;
                }

                if (success)
                {
                    AddStatusTextSuccess($"Success in creating file {GetFileName(newFilePath)}");
                }
                else
                {
                    return;
                }
                
                documents.Add(document);
            }
        }

        private void AddStatusText(string text)
        {
            TextBoxStatus.AppendText(text + Environment.NewLine);
        }

        private void AddStatusText(string text, Color textColor)
        {
            Color defaultSelectionColor = TextBoxStatus.SelectionColor;
            TextBoxStatus.SelectionColor = textColor;
            AddStatusText(text);
            TextBoxStatus.SelectionColor = defaultSelectionColor;
        }

        private void AddStatusTextError(string text)
        {
            AddStatusText(text, _statusErrorColor);
        }

        private void AddStatusTextSuccess(string text)
        {
            AddStatusText(text, _statusSuccessColor);
        }

        private void AddStatusTextWarning(string text)
        {
            AddStatusText(text, _statusWarningColor);
        }

        private string GetFileName(string filePath)
        {
            return filePath.Replace(TextBoxRootFolder.Text + "\\", "");
        }

        private void SetupDocument(Document document)
        {
            StreamWriter writer = new StreamWriter(document.HtmlFile);

            CheckErrorMessage(writer, WriteDocumentTemplateStart(document, writer));

            CheckErrorMessage(writer, WriteDocumentHead(document, writer));

            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "<body>"));
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t<div id=\"container\">"));

            // Header
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t<div id=\"header\">"));

            List<string> lines = GetLines(TextBoxRootFolder.Text + DataPath + MenuLargeFile, out var errorMessage);
            CheckErrorMessage(writer, errorMessage);
            // Set active page
            for (int i = 0; i < lines.Count; ++i)
            {
                var line = lines[i];
                if (line.Contains("<a") && line.Contains(GetFileName(document.HtmlPath)))
                {
                    int index = line.LastIndexOf('>') - 1;
                    lines[i] = line.Insert(index, " active-page");
                }
            }
            // =================================================
            CheckErrorMessage(writer, WriteDocumentLines(document, writer, lines));

            lines = GetLines(TextBoxRootFolder.Text + DataPath + MenuMediumSmallFile, out errorMessage);
            CheckErrorMessage(writer, errorMessage);
            CheckErrorMessage(writer, WriteDocumentLines(document, writer, lines));

            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t</div>"));
            // =================================================

            // Main body
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t<div id=\"body\">"));
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t\t<div class=\"w3-row\">"));

            // Title-tag
            lines = GetLines(TextBoxRootFolder.Text + DataPath + TitleTagFile, out errorMessage);
            CheckErrorMessage(writer, errorMessage);
            CheckErrorMessage(writer, WriteDocumentLines(document, writer, lines));
            // =================================================

            // Main content
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t\t\t<div class=\"w3-row\">"));
            CheckErrorMessage(writer,
                WriteDocumentLine(document, writer, "\t\t\t\t\t<div class=\"w3-col l3 m2 s1\"><p></p></div>"));
            CheckErrorMessage(writer,
                WriteDocumentLine(document, writer,
                    "\t\t\t\t\t<div class=\"w3-col l6 m8 s10 w3-section w3-text-white\">"));

            // TODO: Parse jsf file correctly!
            lines = GetLines(document.Path, out errorMessage);
            CheckErrorMessage(writer, errorMessage);
            CheckErrorMessage(writer, WriteDocumentLines(document, writer, lines));

            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t\t\t\t</div>"));
            CheckErrorMessage(writer,
                WriteDocumentLine(document, writer, "\t\t\t\t\t<div class=\"w3-col l3 m2 s1\"><p></p></div>"));
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t\t\t</div>"));
            // =================================================

            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t\t</div>"));
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t</div>"));
            // =================================================

            // Footer
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t<div id=\"footer\">"));

            lines = GetLines(TextBoxRootFolder.Text + DataPath + FooterFile, out errorMessage);
            CheckErrorMessage(writer, errorMessage);
            CheckErrorMessage(writer, WriteDocumentLines(document, writer, lines));

            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t\t</div>"));
            // =================================================

            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "\t</div>"));
            CheckErrorMessage(writer, WriteDocumentLine(document, writer, "</body>"));

            CheckErrorMessage(writer, WriteDocumentLine(document, writer, DocumentTemplateEnding));

            CheckErrorMessage(writer, FlushDocument(document, writer));
        }

        private void CheckErrorMessage(StreamWriter writer, string message)
        {
            if (!message.Equals(String.Empty))
            {
                writer.Flush();
                writer.Close();
                throw new Exception(message);
            }
        }

        private string WriteDocumentTemplateStart(Document document, StreamWriter writer)
        {
            try
            {
                writer.Write(_documentTemplate);
            }
            catch (Exception e)
            {
                return $"Exception writing to file {GetFileName(document.HtmlPath)}: {e.ToString()} {e.Message}";
            }

            return String.Empty;
        }

        private string WriteDocumentHead(Document document, StreamWriter writer)
        {
            string fullDataPath = TextBoxRootFolder.Text + DataPath + HeadFile;
            List<string> lines = GetLines(fullDataPath, out var errorMessage);

            if (errorMessage.Length != 0)
            {
                return errorMessage;
            }

            return WriteDocumentLines(document, writer, lines);
        }

        private string WriteDocumentLines(Document document, StreamWriter writer, IEnumerable<string> lines)
        {
            string errors = String.Empty;
            foreach (string line in lines)
            {
                string error = WriteDocumentLine(document, writer, line);
                if (error.Length != 0)
                {
                    errors += error + Environment.NewLine;
                }
            }

            return errors;
        }

        private string WriteDocumentLine(Document document, StreamWriter writer, string line)
        {
            try
            {
                writer.WriteLine(line);
            }
            catch (Exception e)
            {
                return $"Exception writing to file {GetFileName(document.HtmlPath)}: {e.ToString()} {e.Message}";
            }

            return String.Empty;
        }

        private string FlushDocument(Document document, StreamWriter writer)
        {
            try
            {
                writer.Flush();
            }
            catch (Exception e)
            {
                return $"Exception flushing file {GetFileName(document.HtmlPath)}: {e.ToString()} {e.Message}";
            }

            try
            {
                writer.Close();
            }
            catch (Exception e)
            {
                return $"Exception closing file {GetFileName(document.HtmlPath)}: {e.ToString()} {e.Message}";
            }

            return String.Empty;
        }

        private List<string> GetLines(string filePath, out string errorMessage)
        {
            List<string> lines = new List<string>();
            try
            {
                lines = new List<string>(File.ReadLines(filePath));
            }
            catch (Exception e)
            {
                errorMessage = $"Received error when reading lines of file {filePath}: {e.ToString()} {e.Message}";
                return lines;
            }

            errorMessage = String.Empty;
            return lines;
        }
    }
}
