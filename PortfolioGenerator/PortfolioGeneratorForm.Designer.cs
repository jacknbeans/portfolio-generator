﻿namespace PortfolioGenerator
{
    partial class PortfolioGeneratorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxRootFolder = new System.Windows.Forms.TextBox();
            this.BrowseRootFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.ButtonRootFolderBrowse = new System.Windows.Forms.Button();
            this.LabelRootFolder = new System.Windows.Forms.Label();
            this.LayoutRootFolder = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.ButtonGenerate = new System.Windows.Forms.Button();
            this.ProgressGeneration = new System.Windows.Forms.ProgressBar();
            this.TextBoxStatus = new System.Windows.Forms.RichTextBox();
            this.LayoutRootFolder.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxRootFolder
            // 
            this.TextBoxRootFolder.Location = new System.Drawing.Point(3, 3);
            this.TextBoxRootFolder.Name = "TextBoxRootFolder";
            this.TextBoxRootFolder.Size = new System.Drawing.Size(590, 26);
            this.TextBoxRootFolder.TabIndex = 0;
            this.TextBoxRootFolder.Text = "D:\\Documents\\coding\\html\\jackreggin.com";
            // 
            // ButtonRootFolderBrowse
            // 
            this.ButtonRootFolderBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonRootFolderBrowse.AutoSize = true;
            this.ButtonRootFolderBrowse.Location = new System.Drawing.Point(599, 3);
            this.ButtonRootFolderBrowse.Name = "ButtonRootFolderBrowse";
            this.ButtonRootFolderBrowse.Size = new System.Drawing.Size(110, 30);
            this.ButtonRootFolderBrowse.TabIndex = 1;
            this.ButtonRootFolderBrowse.Text = "Browse...";
            this.ButtonRootFolderBrowse.UseVisualStyleBackColor = true;
            this.ButtonRootFolderBrowse.Click += new System.EventHandler(this.ButtonRootFolderBrowse_Click);
            // 
            // LabelRootFolder
            // 
            this.LabelRootFolder.AutoSize = true;
            this.LabelRootFolder.Location = new System.Drawing.Point(3, 0);
            this.LabelRootFolder.Name = "LabelRootFolder";
            this.LabelRootFolder.Size = new System.Drawing.Size(97, 20);
            this.LabelRootFolder.TabIndex = 2;
            this.LabelRootFolder.Text = "Root Folder:";
            // 
            // LayoutRootFolder
            // 
            this.LayoutRootFolder.AutoSize = true;
            this.LayoutRootFolder.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LayoutRootFolder.Controls.Add(this.LabelRootFolder);
            this.LayoutRootFolder.Controls.Add(this.flowLayoutPanel2);
            this.LayoutRootFolder.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.LayoutRootFolder.Location = new System.Drawing.Point(12, 12);
            this.LayoutRootFolder.Name = "LayoutRootFolder";
            this.LayoutRootFolder.Size = new System.Drawing.Size(718, 62);
            this.LayoutRootFolder.TabIndex = 3;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flowLayoutPanel2.Controls.Add(this.TextBoxRootFolder);
            this.flowLayoutPanel2.Controls.Add(this.ButtonRootFolderBrowse);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 23);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(712, 36);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // ButtonGenerate
            // 
            this.ButtonGenerate.Location = new System.Drawing.Point(12, 80);
            this.ButtonGenerate.Name = "ButtonGenerate";
            this.ButtonGenerate.Size = new System.Drawing.Size(718, 42);
            this.ButtonGenerate.TabIndex = 4;
            this.ButtonGenerate.Text = "Generate!";
            this.ButtonGenerate.UseVisualStyleBackColor = true;
            this.ButtonGenerate.Click += new System.EventHandler(this.ButtonGenerate_Click);
            // 
            // ProgressGeneration
            // 
            this.ProgressGeneration.Location = new System.Drawing.Point(12, 129);
            this.ProgressGeneration.Name = "ProgressGeneration";
            this.ProgressGeneration.Size = new System.Drawing.Size(718, 23);
            this.ProgressGeneration.TabIndex = 6;
            // 
            // TextBoxStatus
            // 
            this.TextBoxStatus.Enabled = false;
            this.TextBoxStatus.Location = new System.Drawing.Point(12, 158);
            this.TextBoxStatus.Name = "TextBoxStatus";
            this.TextBoxStatus.Size = new System.Drawing.Size(718, 534);
            this.TextBoxStatus.TabIndex = 7;
            this.TextBoxStatus.Text = "";
            // 
            // PortfolioGeneratorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 704);
            this.Controls.Add(this.TextBoxStatus);
            this.Controls.Add(this.ProgressGeneration);
            this.Controls.Add(this.ButtonGenerate);
            this.Controls.Add(this.LayoutRootFolder);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(760, 760);
            this.MinimumSize = new System.Drawing.Size(760, 760);
            this.Name = "PortfolioGeneratorForm";
            this.ShowIcon = false;
            this.Text = "Portfolio Generator";
            this.Load += new System.EventHandler(this.PortfolioGeneratorForm_Load);
            this.LayoutRootFolder.ResumeLayout(false);
            this.LayoutRootFolder.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxRootFolder;
        private System.Windows.Forms.FolderBrowserDialog BrowseRootFolder;
        private System.Windows.Forms.Button ButtonRootFolderBrowse;
        private System.Windows.Forms.Label LabelRootFolder;
        private System.Windows.Forms.FlowLayoutPanel LayoutRootFolder;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button ButtonGenerate;
        private System.Windows.Forms.ProgressBar ProgressGeneration;
        private System.Windows.Forms.RichTextBox TextBoxStatus;
    }
}

